import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('LoginValidAccount'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('AddToChartItem'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('IconChart-1Item'), 0)

Mobile.tap(findTestObject('ChartPage/btnProceedToCheckout'), 0)

Mobile.tap(findTestObject('ChartPage/payment/inputFullName'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputFullName'), 'Randika')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/inputAddressLine1'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputAddressLine1'), 'Jalan Kota')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/inputAddressLine2'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputAddressLine2'), 'Jalan Kota')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/inputCity'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputCity'), 'Medan')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/inputState'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputState'), 'SumatraUtara')

Mobile.hideKeyboard()

Mobile.scrollToText('Zip Code*', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('ChartPage/payment/inputZipCode'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputZipCode'), '20131')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/inputCountry'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/inputCountry'), 'Indonesia')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/btnStatePayment'), 0)

Mobile.verifyElementExist(findTestObject('ChartPage/payment/titlePaymentMethod'), 0)


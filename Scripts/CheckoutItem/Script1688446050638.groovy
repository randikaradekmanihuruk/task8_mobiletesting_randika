import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('ProceedToCheckout'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('ChartPage/payment/method/InputFullName'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/method/InputFullName'), 'Randika R M')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/method/inputCardNumber'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/method/inputCardNumber'), '345656783456123')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/method/inputExpirationDate'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/method/inputExpirationDate'), '0325')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/method/inputSecurityCode'), 0)

Mobile.sendKeys(findTestObject('ChartPage/payment/method/inputSecurityCode'), '123')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('ChartPage/payment/method/btnReviewOrder'), 0)

Mobile.delay(0.3, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('ChartPage/payment/method/btnReviewOrder'), 0)

Mobile.verifyElementExist(findTestObject('ChartPage/payment/method/titleCheckout'), 0)

